<?php get_header();
$template_url=get_template_directory_uri();
the_post();
the_content();
$args = array(
    'numberposts' => 4,
    'category' => wp_get_post_categories(get_the_ID()),
    'orderby'     => 'date',
    'order'       => 'DESC',

    'exclude'     => array(get_the_ID()),
    'post_type'   => 'post',
    'suppress_filters' => true,
);
$posts=get_posts($args);

?>
    <section class="breadcrumb">
        <div class="container">

            <a href="/"><img src="<?=get_template_directory_uri()?>/app/img/arrowleft.svg" alt=""> Back</a>
            <div class="links">
                <strong>Home / Help & Faq / </strong><span><?php the_title(); ?></span>
            </div>
        </div>
    </section>
    <section class="search banner" style="background-image: url(<?=get_template_directory_uri()?>/app/img/Help_FAQ.jpg)">
        <div class="container">
            <h1 class="block_title">Help & Faq</h1>
            <div class="divider_block"></div>
            <div class="wrap_search">
                <?php get_search_form( ); ?>
            </div>
        </div>
    </section>
    <section class="rubrics accordion-section">
        <div class="container">
            <div>
                <h1><?php the_title(); ?></h1>
                <div id="accordion" class="accordion">
                    <?php $post_counter=1; if(get_field('accordeon')): ?>
                        <?php while(has_sub_field('accordeon')): ?>
                            <h3 id="s<?=$post_counter?>" class="accordion-title"><?php the_sub_field('name'); ?></h3>
                            <div class="accordion-children s<?=$post_counter?>"><?php the_sub_field('text'); ?></div>
                        <?php $post_counter++; endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="contact_form">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
