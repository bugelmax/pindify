<?php get_header();

$template_url=get_template_directory_uri();
$args = array(
    'numberposts' => 13,

    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'post_type'   => 'post',
    'suppress_filters' => false,
);
$posts=get_posts($args);
?>
    <section class="banner success" style="background-image: url(<?=(!empty(get_post_thumbnail_id($obj->ID)))?GetImageUrl(get_post_thumbnail_id($obj->ID),'full'):null?>)">
        <div class="container">
            <h1 class="block_title"><?php the_title(); ?></h1>
            <div class="divider_block"></div>
            <p class="block_subtitle"><?php the_field('subtitle'); ?></p>
        </div>
    </section>
    <section class="posts">
        <?php foreach ($posts as $obj):?>
            <div class="post__item" style="background-image: url('<?=(!empty(get_post_thumbnail_id($obj->ID)))?GetImageUrl(get_post_thumbnail_id($obj->ID),'medium'):null?>')">
                <div>
                    <h3 class="title">
                        <a class="" href="<?=get_permalink($obj->ID)?>">
                            <?php echo $obj->post_title;?>
                        </a>
                    </h3>
                    <?php
                        $post_tags = get_the_terms( $obj->ID, 'post_tag' );
                        $html = "<nav>";
                        foreach ($post_tags as $tag){
                            $tag_link = get_tag_link($tag->term_id);
                            $html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='post_item_tags_name {$tag->slug}'>";
                            $html .= "{$tag->name}</a>";
                        }
                        $html .= "</nav>";
                        echo $html;
                    ?>
                </div>
            </div>
        <?php
            endforeach;
        ?>
    </section>
<?php get_footer(); ?>
