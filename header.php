<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>Pindify - <?php the_title(); ?></title>
    <meta name="theme-color" content="#ffffff">
    <link rel="shortcut icon" type="image/x-icon" href="<?=get_template_directory_uri()?>/app/img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,700|Roboto:300,300i,400,500" rel="stylesheet">
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/app/css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/app/css/style.min.css">
    <?php wp_head(); ?>
    <?php global $post; ?>
</head>
<body class="content <?php if (is_front_page() ): ?>main <?php elseif (is_page(36) ): ?> guide_page <?php elseif (is_search()): ?> search_page other<?php else: ?> other <?php endif; ?>">
    <header>
        <div class="container">
            <nav class="header_menu left">
                <a href="/" class="logo icon">
                    <svg viewBox="0 0 62 70" aria-label="Pindify" class="icon___1OCig" ><path d="M30.894 37.927c-3.412 0-6.178-2.83-6.178-6.322 0-3.49 2.766-6.32 6.178-6.32 3.413 0 6.179 2.83 6.179 6.32 0 3.492-2.766 6.322-6.179 6.322zm30.895-6.321c0 .095-.007.189-.007.284 0 .025.007.047.007.071V63.68c0 .262-.031.517-.076.769C61.338 67.572 58.765 70 55.61 70c-.478 0-.938-.068-1.385-.173l-.045.002-25.019-6.87.004-.03c-2.565-.769-4.45-3.166-4.45-6.04 0-3.49 2.767-6.32 6.18-6.32 10.237 0 18.536-8.49 18.536-18.963s-8.3-18.964-18.537-18.964c-10.237 0-18.536 8.49-18.536 18.964v32.073c0 3.49-2.767 6.321-6.18 6.321C2.768 70 0 67.17 0 63.68V31.96c0-.024.007-.046.007-.07 0-.096-.007-.19-.007-.285C0 14.15 13.832 0 30.894 0 47.957 0 61.79 14.15 61.79 31.606z" fill="#F5F5F5" fill-rule="evenodd"></path></svg>
                    <?php if( is_page( 21 ) ) { ?>
                        <span>Help & FAQ</span>
                    <?php } elseif ( is_page(  36) ) { ?>
                        <span>Getting started</span>
                    <?php } elseif ( is_page(  39) ) { ?>
                        <span>Stories</span>
                    <?php } elseif ( is_search() ) { ?>
                        <span>Help & FAQ</span>
                    <?php } elseif ( in_category('success-stories') ) { ?>
                        <span>Stories</span>
                    <?php } else { ?>
                        <span>Help & FAQ</span>
                    <?php } ?>
                </a>
                <?php if (false):?>
                    <a class="mobile" href="<?php the_field('link_1_url', 'option'); ?>"><?php the_field('link_1_name', 'option'); ?></a>
                    <a class="mobile" href="<?php the_field('link_2_url', 'option'); ?>"><?php the_field('link_2_name', 'option'); ?></a>
                    <a class="mobile italic" href="<?php the_field('link_3_url', 'option'); ?>"><?php the_field('link_3_name', 'option'); ?></a>
                <?php endif;?>
            </nav>
            <?php if (false):?>
                <nav class="header_menu right">
                    <a class="icon sign_in mobile" href="<?php the_field('login', 'option'); ?>">
                        <svg viewBox="0 0 24 24" aria-label="SignUp" class="icon___1OCig icon___3dCii" style="width: 24px; height: 24px;">
                            <path d="M24 .38v23.25c0 .2-.17.37-.38.37H9.38a.37.37 0 1 1 0-.75h13.88V.75H9.37a.38.38 0 0 1 0-.75h14.26c.2 0 .37.17.37.38zm-3.7 11.96l-8.29 8.3a.36.36 0 1 1-.51-.51l7.73-7.75H3.38a.38.38 0 0 1 0-.76h15.85L11.5 3.87a.37.37 0 0 1 .51-.51l8.35 8.36.02.02a.37.37 0 0 1-.08.6z" fill="#F5F5F5" fill-rule="evenodd"></path></svg>
                        <strong class="" style="font-size: 1.4rem;">Login</strong>
                    </a>
                    <a class="button sign_up" type="button" href="<?php the_field('sign_in', 'option'); ?>">
                        <strong class="mobile_hide">Sign up for free</strong>
                    </a>
                    <button type="button" class="button menu-button">
                        <svg viewBox="0 0 24 24" aria-label="Menu" class="icon menu-icon active" style="width: 24px; height: 24px;">
                            <path d="M.5 5a.5.5 0 0 1 0-1h23a.5.5 0 1 1 0 1H.5zm0 8a.5.5 0 1 1 0-1h23a.5.5 0 1 1 0 1H.5zm0 8a.5.5 0 1 1 0-1h23a.5.5 0 1 1 0 1H.5z" fill="#F5F5F5" fill-rule="evenodd">
                            </path>
                        </svg>
                        <svg viewBox="0 0 24 24" aria-label="Close" class="icon menu-icon" style="width: 24px; height: 24px;"><path d="M12 11.2929L23.1464.1464a.5.5 0 0 1 .7072.7072L12.707 12l11.1465 11.1464a.5.5 0 0 1-.7072.7072L12 12.707.8536 23.8536a.5.5 0 0 1-.7072-.7072L11.293 12 .1464.8536A.5.5 0 0 1 .8536.1464L12 11.293z" fill="#F5F5F5" fill-rule="nonzero"></path></svg>
                    </button>
                </nav>
            <?php endif;?>
            <a href="https://pindify.com/" class="back">
                <span class="desktop">Back to Pindify.com</span>
                <span class="mobile">Pindify.com</span>
            </a>
            <div class="mobile_menu__container">
                <nav class="mobile_menu">
                    <a href="<?php the_field('market_link_url', 'option'); ?>"><img src="<?=get_template_directory_uri()?>/app/img/market.svg" alt="<?php the_field('market_link_name', 'option'); ?>">
                        <strong><?php the_field('market_link_name', 'option'); ?></strong></a>
                    <a href="<?php the_field('link_1_url', 'option'); ?>"><?php the_field('link_1_name', 'option'); ?></a>
                    <a href="<?php the_field('link_2_url', 'option'); ?>"><?php the_field('link_2_name', 'option'); ?></a>
                    <a href="<?php the_field('link_3_url', 'option'); ?>"><?php the_field('link_3_name', 'option'); ?></a>
                    <a href="/guide">101 Get started</a>
                    <a href="/success-stories">Success Stories</a>
                    <a href="<?php the_field('about', 'option'); ?>">About</a>
                    <a class="icon sign_in mobile" href="<?php the_field('login', 'option'); ?>">
                        <svg viewBox="0 0 24 24" aria-label="SignUp" class="icon___1OCig icon___3dCii" style="width: 24px; height: 24px;">
                            <path d="M24 .38v23.25c0 .2-.17.37-.38.37H9.38a.37.37 0 1 1 0-.75h13.88V.75H9.37a.38.38 0 0 1 0-.75h14.26c.2 0 .37.17.37.38zm-3.7 11.96l-8.29 8.3a.36.36 0 1 1-.51-.51l7.73-7.75H3.38a.38.38 0 0 1 0-.76h15.85L11.5 3.87a.37.37 0 0 1 .51-.51l8.35 8.36.02.02a.37.37 0 0 1-.08.6z" fill="#F5F5F5" fill-rule="evenodd"></path></svg>
                        <strong class="" style="font-size: 1.4rem;">Login</strong>
                    </a>
                </nav>
            </div>
        </div>
    </header>
    <main>