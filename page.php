<?php get_header();
$template_url=get_template_directory_uri();
the_post();

?>
<div>
    <section class="about_us">
        <div class="container">
            <?php the_content(); ?>
        </div>
    </section>
</div>
<?php get_footer(); ?>
