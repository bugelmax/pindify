<?php get_header();
global $query_string;
wp_parse_str( $query_string, $search_query );
$query = new WP_Query(array_merge($search_query,array('post_type' =>array('post','rubrics'))));
$template_url=get_template_directory_uri();

?>
    <section class="search banner" style="background-image: url('<?=get_theme_file_uri()?>/app/img/Help_FAQ.jpg')">
        <div class="container">
            <h1 class="block_title">Help & Faq</h1>
            <div class="divider_block"></div>
            <div class="wrap_search">
                <?php get_search_form( ); ?>
            </div>
        </div>
    </section>
    <section class="rubrics">
        <div class="container">
            <?php if ( $query->have_posts() ) :  ?>
                <div class="search_result">
                    <p>Search result: </p>
                    <?php foreach ($query->get_posts() as $post): ?>
                        <a href="<?=get_permalink($post->ID);?>">
                            <span class="title"><?php echo $post->post_title; ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>

                <?php else : ?>
                <div class="search_result">
                    <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
                </div>
            <?php endif; ?>
            <div class="contact_form">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>