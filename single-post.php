<?php get_header();
$template_url=get_template_directory_uri();
the_post();
$category = get_the_category();
?>

<section class="banner" style="background-image: url(<?=(!empty(get_post_thumbnail_id($obj->ID)))?GetImageUrl(get_post_thumbnail_id($obj->ID),'full'):null?>)">
    <div class="container">
        <p class="category_name"><?php echo $category[0]->cat_name; ?></p>
        <h1 class="block_title"><?php the_title(); ?></h1>
        <div class="divider_block"></div>
        <p class="block_subtitle"><?php the_field('subtitle'); ?></p>
    </div>
</section>
<section class="page_content">
        <?php if( have_rows('content') ): ?>
            <?php while( have_rows('content') ): the_row(); ?>
                <div class="container">
                    <div class="divider_block"></div>
                    <div class="paragraph_wrapper">
                        <?php if( have_rows('text') ): ?>
                            <?php while( have_rows('text') ): the_row(); ?>
                                <p><?php the_sub_field('paragraph'); ?></p>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php $image = get_sub_field('image'); if ($image): ?>
                    <img src="<?php echo $image; ?>" alt="">
                <?php endif; ?>
            <?php endwhile;?>
        <?php endif; ?>
</section>
<?php get_footer(); ?>
