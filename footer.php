    </main>
    <footer class="footer">
        <div class="container">
            <div class="left">
                <p class="text___2ZyXv roboto___19fRf light___2_Wzp p___3uCmO big___cQWJx">
                    <strong class="text___2ZyXv roboto___19fRf light___2_Wzp">Pindify</strong> is the marketplace where creative providers build their fan base and interact with their superfans by creating and sharing original content.</p>
                <p class="text___2ZyXv roboto___19fRf light___2_Wzp p___3uCmO big___cQWJx">Our vision is for Providers in all genres to earn enough to pursue their artistic callings. Content on Pindify emphasizes the raw, behind-the-scenes experience. <strong class="text___2ZyXv roboto___19fRf light___2_Wzp">Our model means that even smaller creators can catch a break.﻿﻿﻿</strong>
                </p>
                <img class="img___1qJns" src="https://featurecontent.pindify.com/uploads/public/logo-landscape.png">
            </div>
            <div class="right">
                <nav class="nav___3r7q1 margin___Q0VVM">
                    <strong class="text___2ZyXv roboto___19fRf  " style="font-size: 1.8rem;">WEBSITE</strong>
                    <a class="a___sUreg" href="<?php the_field('be_heard__get_paid', 'option'); ?>">
                        <strong class="text___2ZyXv  " style="font-size: 1.8rem;">Be Heard Get Paid</strong>
                    </a>
                    <div class="sub-nav">
                        <a class="a___sUreg" href="<?php the_field('income', 'option'); ?>">
                            <span class="text___2ZyXv   " style="font-size: 1.8rem;">Income Model</span>
                        </a>
                    </div>
                    <a class="" href="<?php the_field('all_access', 'option'); ?>">
                        <strong class="text___2ZyXv roboto___19fRf  " style="font-size: 1.8rem;">All Access</strong>
                    </a>
                    <a class="a___sUreg" href="<?php the_field('backstage', 'option'); ?>">
                        <strong class="   " style="font-size: 1.8rem;">Backstage</strong>
                    </a>
                    <div class="sub-nav margin___Q0VVM">
                        <a class="a___sUreg" href="/success-stories">
                            <span class="text___2ZyXv   " style="font-size: 1.8rem;">Stories</span>
                        </a>
                        <a class="a___sUreg" href="/guide">
                            <span class="   " style="font-size: 1.8rem;">Providers Guide</span>
                        </a>
                        <a class="a___sUreg" href="/">
                            <span class="text___2ZyXv roboto___19fRf  " style="font-size: 1.8rem;">Help &amp; FAQ</span>
                        </a>
                        <a class="a___sUreg" href="<?php the_field('about', 'option'); ?>">
                            <span class="   " style="font-size: 1.8rem;">About Pindify</span>
                        </a>
                    </div>
                </nav>
                <nav class="nav___3r7q1 margin___Q0VVM">
                    <strong class="text___2ZyXv   " style="font-size: 1.8rem;">OTHER SITES</strong>
                    <a class="a___sUreg" href="<?php the_field('pindex', 'option'); ?>">
                        <strong class="text___2ZyXv   " style="font-size: 1.8rem;">Pindex</strong>
                    </a>
                    <a class="a___sUreg" href="<?php the_field('investors', 'option'); ?>">
                        <strong class="text___2ZyXv   " style="font-size: 1.8rem;">Investors</strong>
                    </a>
                </nav>
                <nav class="nav___3r7q1 margin___Q0VVM">
                    <strong class="text___2ZyXv roboto___19fRf  " style="font-size: 1.8rem;">FOLLOW US</strong>
                    <div class="socials">
                        <a class="a___sUreg icon___3HxnD icon___2Jho-" href="<?php the_field('facebook', 'option'); ?>">
                            <svg viewBox="0 0 30 30" aria-label="FacebookCircle" class="icon___1OCig" style="width: 44px; height: 44px;">
                                <g fill="#E3E3E3" fill-rule="evenodd">
                                    <path d="M0 15C0 6.716 6.716 0 15 0c8.284 0 15 6.716 15 15 0 8.284-6.716 15-15 15-8.284 0-15-6.716-15-15z" fill="#3B5998"></path>
                                    <path d="M15.841 23.415v-8.05h2.165l.287-2.774H15.84l.004-1.388c0-.724.067-1.112 1.08-1.112h1.352V7.317h-2.165c-2.6 0-3.515 1.346-3.515 3.609v1.665h-1.621v2.774h1.62v8.05h3.245z" fill="#FFF"></path>
                                </g>
                            </svg>
                        </a>
                        <a class="a___sUreg icon___3HxnD icon___2Jho-" href="<?php the_field('twitter', 'option'); ?>">
                            <svg viewBox="0 0 30 30" aria-label="TwitterCircle" class="icon___1OCig" style="width: 44px; height: 44px;">
                                <g fill="#E3E3E3" fill-rule="evenodd"><path d="M0 15C0 6.716 6.716 0 15 0c8.284 0 15 6.716 15 15 0 8.284-6.716 15-15 15-8.284 0-15-6.716-15-15z" fill="#55ACEE"></path>
                                    <path d="M14.55 12.192l.032.52-.524-.064c-1.91-.244-3.578-1.07-4.995-2.458l-.692-.688-.179.508c-.377 1.134-.136 2.33.651 3.136.42.444.325.508-.399.243-.252-.085-.472-.148-.493-.116-.073.074.179 1.038.378 1.419.273.53.829 1.049 1.437 1.356l.514.243-.608.011c-.588 0-.609.01-.546.233.21.689 1.039 1.42 1.962 1.737l.65.223-.566.339a5.907 5.907 0 0 1-2.812.784c-.472.01-.86.053-.86.084 0 .106 1.28.7 2.025.932 2.235.689 4.89.392 6.883-.783 1.416-.837 2.833-2.5 3.494-4.11.357-.858.713-2.426.713-3.178 0-.487.032-.55.62-1.133.346-.34.67-.71.734-.816.105-.201.094-.201-.44-.021-.893.317-1.019.275-.578-.202.325-.339.713-.953.713-1.133 0-.032-.157.021-.335.117-.19.106-.609.264-.924.36l-.566.18-.514-.35c-.284-.19-.682-.402-.892-.466-.535-.148-1.354-.127-1.836.043-1.312.476-2.14 1.705-2.046 3.05z" fill="#FFF"></path>
                                </g>
                            </svg>
                        </a>
                        <a class="a___sUreg icon___3HxnD icon___2Jho-" href="<?php the_field('instagram', 'option'); ?>">
                            <svg viewBox="0 0 24 24" aria-label="InstagramCircle" class="icon___1OCig" style="width: 44px; height: 44px;">
                                <g fill="#E3E3E3">
                                    <path d="M12 24C5.373 24 0 18.627 0 12S5.373 0 12 0s12 5.373 12 12-5.373 12-12 12zm4.84-15.818a1.023 1.023 0 1 0-2.045 0 1.023 1.023 0 0 0 2.046 0zm-4.567 6.746a2.656 2.656 0 1 1 0-5.31 2.656 2.656 0 0 1 0 5.31zm0-6.746a4.09 4.09 0 1 0 0 8.182 4.09 4.09 0 0 0 0-8.182zm1.26 11.998a37.89 37.89 0 0 0 1.997-.046c.842-.038 1.416-.172 1.92-.367.52-.202.96-.473 1.4-.912.44-.44.71-.88.912-1.4.195-.504.329-1.078.367-1.92.039-.843.048-1.112.048-3.26 0-2.147-.01-2.416-.048-3.26-.038-.841-.172-1.416-.367-1.919a3.876 3.876 0 0 0-.912-1.4c-.44-.44-.88-.71-1.4-.912-.504-.196-1.078-.33-1.92-.368-.843-.038-1.112-.047-3.26-.047-2.147 0-2.416.009-3.26.047-.841.039-1.416.172-1.919.368-.52.202-.96.472-1.4.912-.44.44-.71.88-.912 1.4-.196.503-.33 1.078-.368 1.92-.038.843-.047 1.112-.047 3.26 0 2.147.009 2.416.047 3.26.039.84.172 1.415.368 1.918.202.52.472.961.912 1.4.44.44.88.71 1.4.913.503.195 1.078.329 1.92.367.639.03.948.042 1.996.046h2.526zM12.27 5.793c2.111 0 2.361.008 3.195.046.771.035 1.19.164 1.468.273.37.143.633.314.91.591.276.277.447.54.59.91.11.278.238.696.273 1.467.038.834.046 1.084.046 3.195 0 2.111-.008 2.361-.046 3.195-.035.771-.164 1.19-.272 1.468a2.45 2.45 0 0 1-.592.91 2.45 2.45 0 0 1-.909.59c-.278.11-.697.238-1.468.273-.833.038-1.083.046-3.195.046-2.111 0-2.361-.008-3.195-.046-.77-.035-1.19-.164-1.468-.272a2.45 2.45 0 0 1-.909-.592 2.45 2.45 0 0 1-.591-.909c-.109-.278-.237-.697-.273-1.468-.038-.834-.046-1.084-.046-3.195 0-2.11.008-2.361.046-3.195.036-.77.164-1.19.273-1.468a2.45 2.45 0 0 1 .591-.909 2.45 2.45 0 0 1 .91-.591c.278-.109.696-.238 1.467-.273.834-.038 1.084-.046 3.195-.046z" fill="#DB2C74" fill-rule="evenodd"></path>
                                </g>
                            </svg>
                        </a>
                    </div>
                </nav>
            </div>
        </div>
    </footer>
<?php wp_footer(); ?>
    <script src="<?=get_template_directory_uri()?>/app/libs/jquery.min.js"></script>
    <script src="<?=get_template_directory_uri()?>/app/libs/jquery-ui.min.js"></script>
    <script src="<?=get_template_directory_uri()?>/app/js/common.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.searchform input[name="s"]').on('keyup',function () {
                $('ul.search_result').removeClass('active');
                $('ul.search_result').empty();
                var val=$(this).val();
                if (($(this).val()).length>=2){
                    $.ajax({
                        url: window.location.origin + "/wp-admin/admin-ajax.php",
                        type: "POST",
                        data: {
                            "action": "search_helper",
                            "data": {
                                "search": val,
                            },
                        },
                        dataType: "json",
                        async: true,
                        cache: false,
                        timeout: 30000,
                        beforeSend: function (xhr) {
                        },
                        success: function (response) {
                            //if success then remove
                            try {
                                if (response.status == true) {

                                    for (var key in response.data){

                                        $('ul.search_result').append('<li><a href="'+response.data[key].url+'">'+response.data[key].post_title+'</a><li>')
                                    }
                                    $('ul.search_result').addClass('active');
                                }
                                else {

                                }
                            }
                            catch (exception) {
                            }
                        }
                    });
                }
            })
        })
    </script>
</body>
</html>
