var gulp           = require('gulp'),
		gutil          	= require('gulp-util' ),
		sass           	= require('gulp-sass'),
		browserSync    	= require('browser-sync'),
		concat         	= require('gulp-concat'),
		uglify         	= require('gulp-uglify'),
		cssnano 		= require('gulp-cssnano'),
		cleanCSS    	= require('gulp-clean-css'),
		rename         	= require('gulp-rename'),
		autoprefixer   	= require('gulp-autoprefixer'),
		notify         	= require("gulp-notify");

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false,
	});
});


gulp.task('js-libs', function() {
	return gulp.src([
		'app/libs/jquery.min.js',
		'app/libs/jquery-ui.min.js'
		])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});
gulp.task('js', function() {
	return gulp.src('app/js/common.js')
	.pipe(concat('common.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('sass', function() {
	return gulp.src('app/scss/**/*.scss')
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
	.pipe(cssnano())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['sass', 'js-libs', 'js', 'browser-sync'], function() {
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch(['libs/**/*.js', 'app/js/common.js'], ['js']);
	gulp.watch('app/*.html', browserSync.reload);
});

gulp.task('default', ['watch']);
