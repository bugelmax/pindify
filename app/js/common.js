$(document).ready(function() {

    $(".menu-button").on("click", function() {
        $(this).children("svg").toggleClass("active")
    });
    $(".header_menu .button.menu-button").on("click", function() {
        $(".mobile_menu__container").toggleClass("show")
    });
    $(".paragraph_wrapper").each(function() {
        1 < $(this).find("p").length && $(this).addClass("two")
    });
    $(".wpcf7-textarea").keyup(function() {
        $('input[type="submit"]').addClass("active")
    });
    $(".wpcf7-textarea").focusout(function() {
        $('input[type="submit"]').removeClass("active")
    });
    $( ".accordion" ).accordion({
        // fillSpace:true,
        active: false,
        collapsible: true,
        heightStyle: "content"
    });

    // $('.accordion .accordion-children').slideUp();
    // $('.accordion .accordion-title').on('click', f_acc);
    //
    // function f_acc(){
    //     // var time = 0;
    //     $('.accordion .accordion-children').not($(this).next()).slideUp();
    //     $(this).next().slideToggle();
    // }
    var containsParams = window.location.href;
    var tabId = containsParams.slice(-1);
    console.log(containsParams, tabId);
    $('.rubrics .accordion').accordion({
        active: ''+tabId+''-1
    });

    function submitButtonValidation() {
        var text = $('.wpcf7-textarea').val();
        var email = $('.wpcf7-email').val();
        var textValid = false;
        var emailValid = false;
        if(text.length < 15) {
            textValid = false;
            $('.wpcf7-textarea').addClass('error');
        } else {
            textValid = true;
            $('.wpcf7-textarea').removeClass('error');
        }
        if(IsEmail(email)==false){
            emailValid = false;
            $('.wpcf7-email').addClass('error');
            $('div.wpcf7-response-output').removeClass('.wpcf7-display-none').text('Invalid email address');
        } else {
            emailValid = true;
            $('.wpcf7-email').removeClass('error');
        }
        return textValid && emailValid ? true : false;
    }

    $('.wpcf7-textarea').blur(function(){
        var text = $('.wpcf7-textarea').val();
        if(text !== '' && text.length < 1) {
            $('.wpcf7-textarea').addClass('error');
        } else {
            $('.wpcf7-textarea').removeClass('error');
        }
    })
    $('.wpcf7-email').blur(function(){
        var email = $('.wpcf7-email').val();
        if(email !== '' && IsEmail(email)==false){
            $('.wpcf7-email').addClass('error');
        } else {
            $('.wpcf7-email').removeClass('error');
        }
    })
    $('.wpcf7-submit').click(function(){
        return submitButtonValidation();
    });

    // $(document).on('keypress', '#searchform input', function() {
    //     var value = $(this).val();
    //     if (value.length >= 2) {
    //         $('.search_result').addClass('active');
    //     } else {
    //         $('.search_result').removeClass('active');
    //     }
    // });
    // $('.search_result a').on('click', function(e){
    //     e.preventDefault();
    //     console.log($(this).text());
    //     var result = $(this).text();
    //     $('#searchform input').val(result)
    // })
});
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
        return false;
    }else{
        return true;
    }
};