<?php
$categories = get_categories( array(
    'type'         => 'rubrics',
    'child_of'     => 0,
    'parent'       => '',
    'orderby'      => 'name',
    'order'        => 'ASC',
    'hide_empty'   => 0,
    'hierarchical' => 1,
    'exclude'      => '',
    'include'      => '',
    'number'       => 0,
    'taxonomy'     => 'category',
    'pad_counts'   => false,
) );
?>
<?php get_header(); ?>

    <section class="search banner" style="background-image: url(<?=(!empty(get_post_thumbnail_id($obj->ID)))?GetImageUrl(get_post_thumbnail_id($obj->ID),'full'):null?>)">
        <div class="container">
            <h1 class="block_title">Help & Faq</h1>
            <div class="divider_block"></div>
            <div class="wrap_search">
                <?php get_search_form( ); ?>
            </div>
        </div>
    </section>
    <section class="rubrics">
    <div class="container">
        <div class="rubrics_list">
            <?php foreach ($categories as $category_obj):
                if ($category_obj->cat_ID!=1):
                ?>
                <?php
                $posts=GetPostsByTaxonomy('category',$category_obj->slug,'rubrics');
                foreach ($posts as $post_arr):?>
                <div class="rubrics_list__item">
                    <a href="<?=get_permalink($post_arr['ID']);?>">
                        <img src="<?=get_the_post_thumbnail_url($post_arr['ID'])?>" alt="<?=$post_arr['post_title']?>">
                        <h2 class="title"><?=$post_arr['post_title']?></h2>
                    </a>
                </div>
                <?php endforeach; ?>
            <? endif;endforeach; ?>
            <div class="rubrics_list__item">
                <a href="<?php the_field('copyright_link', 'option'); ?>">
                    <img src="<?php the_field('copyright_image', 'options'); ?>" alt="<?php the_field('copyright_title', 'options'); ?>">
                    <h2 class="title"><?php the_field('copyright_title', 'options'); ?></h2>
                </a>
            </div>
        </div>
        <div class="contact_form">
            <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
