<?php get_header();
the_post();
?>

    <section class="banner guide_page" style="background-image: url(<?=(!empty(get_post_thumbnail_id($obj->ID)))?GetImageUrl(get_post_thumbnail_id($obj->ID),'full'):null?>)">
        <div class="container">
            <h1 class="block_title"><?php the_title(); ?></h1>
            <div class="divider_block"></div>
            <p class="block_subtitle"><?php the_field('subtitle'); ?></p>
        </div>
    </section>
    <article class="get_started common_style_articles">
        <div class="container">
            <section class="left">
                <img class="big_image" src="<?php the_field('get_started_big_image'); ?>" alt="">
                <img class="small_image"src="<?php the_field('get_started_small_image'); ?>" alt="">
            </section>
            <section class="right">
                <h2 class="block_title"><?php the_field('get_started_title');  ?></h2>
                <div class="divider_block"></div>
                <p class="block_subtitle"><?php the_field('get_started_text'); ?></p>
                <div class="accordion">
                    <?php $post_counter=1; if(get_field('get_started_accordion')): ?>
                        <?php while(has_sub_field('get_started_accordion')): ?>
                            <h3 class="accordion-title">
                                <img src="<?php the_sub_field('icon'); ?>" alt="">
                                <?php the_sub_field('title'); ?>
                            </h3>
                            <div class="accordion-children">
                                <?php the_sub_field('text'); ?>
                            </div>
                        <?php $post_counter++; endwhile; ?>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </article>
    <article class="be_heard common_style_articles">
        <div class="container">
            <section class="left">
                <h2 class="block_title"><?php the_field('be_heard_title');  ?></h2>
                <div class="divider_block"></div>
                <p class="block_subtitle"><?php the_field('be_heard_text'); ?></p>
                <div class="accordion">
                    <?php $post_counter=5; if(get_field('be_heard_accordion')): ?>
                        <?php while(has_sub_field('be_heard_accordion')): ?>
                            <h3 class="accordion-title">
                                <img src="<?php the_sub_field('icon'); ?>" alt="">
                                <?php the_sub_field('title'); ?>
                            </h3>
                            <div class="accordion-children">
                                <?php the_sub_field('text'); ?>
                            </div>
                        <?php $post_counter++; endwhile; ?>
                    <?php endif; ?>
                </div>
            </section>
            <section class="right">
                <div class="big_image image">
                    <div class="square" style="background-image: url('<?php the_field('be_heard_big_image'); ?>')"></div>
                </div>
                <div class="small_image image">
                    <div class="square" style="background-image: url('<?php the_field('be_heard_small_image'); ?>')"></div>
                </div>
                <div class="tiny_image image">
                    <div class="square" style="background-image: url('<?php the_field('be_heard_tiny_image'); ?>')"></div>
                </div>
            </section>
        </div>
    </article>
    <article class="common_style_articles get_paid">
        <div class="container">
            <section class="left">
                <img class="get_paid_big_image"src="<?php the_field('get_paid_image');  ?>" alt="">
            </section>
            <section class="right">
                <h2 class="block_title"><?php the_field('get_paid_title');  ?></h2>
                <div class="divider_block"></div>
                <p class="block_subtitle"><?php the_field('get_paid_text'); ?></p>
                <div class="accordion">
                    <?php $post_counter=10; if(get_field('get_paid_accordion')): ?>
                        <?php while(has_sub_field('get_paid_accordion')): ?>
                            <h3 class="accordion-title">
                                <img src="<?php the_sub_field('icon'); ?>" alt="">
                                <?php the_sub_field('title'); ?>
                            </h3>
                            <div class="accordion-children">
                                <?php the_sub_field('text'); ?>
                            </div>
                        <?php $post_counter++; endwhile; ?>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </article>
    <section>
        <div class="container">
            <hr>
        </div>
    </section>
    <section class="help">
        <div class="container">
            <?php the_content(); ?>
        </div>
    </section>

<?php get_footer(); ?>

