<?php

function pindify_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on redtag, use a find and replace
     * to change 'redtag' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'pindify', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    add_theme_support( 'title-tag' );

    register_nav_menus( array(
        'main-menu' => 'main page',
        'second_menu' => 'other page',
    ) );
}
add_action( 'after_setup_theme', 'pindify_setup' );

function show_image($id,$size='full',$show_baner=false){
    if ($show_baner) {
        $image=image_downsize( $id, $size );
        return str_replace('/wp-content/uploads/','/images/',$image[0]);
    }
    else
        return str_replace('/wp-content/uploads/','/images/',get_the_post_thumbnail_url( $id,$size ));
}

function GetImageUrl($id=0,$size='full'){
    $temp=image_downsize($id,$size);
    return $temp[0];
}

function GetCategoriesByTaxonomy($taxonomy='category',$hide_empty = false, $args = false)
{
    try{
        if ($args)
            $temp=array_merge($args,array('taxonomy' => $taxonomy, 'hide_empty' => $hide_empty));
        else $temp=array('taxonomy' => $taxonomy, 'hide_empty' => $hide_empty);
    }
    catch (Exception $e){
        $temp=array('taxonomy' => $taxonomy, 'hide_empty' => $hide_empty);
    }
    $categories = get_terms($temp);

    if (is_wp_error($categories)) return false;
    if (count($categories) > 0) {
        foreach ($categories as $key => $obj) {
            $obj = (array)$obj;
            $obj['thumbnail'] = GetImageByCategory($obj["term_id"],'thumbnail_id','medium');
            $obj['url']=get_term_link($obj["term_id"]);
            /*      if ($hide_empty)
                      if (($obj['count']==0)&&(filter_var($obj["description"], FILTER_VALIDATE_URL)===false)) unset ($categories[$key]); //the same as 'hide_empty' => true
                  else*/
            $categories[$key] = (object)$obj;


        }
    } else return false;
    return $categories;
}

function GetCategoryObjectByName($taxonomy='category',$cat_name=''){
    $cat=GetCategoriesByTaxonomy($taxonomy);

    foreach ($cat as $obj)
        if ($obj->slug==$cat_name) return $obj;

    return false;
}

function GetPostsByTaxonomy($taxonomy='category',$sub_cat='',$post_type='post',$add_thumbnail='false')
{

    $result_posts = get_posts(
        array(
            'posts_per_page' => 100500,
            'post_type' => $post_type,
            'tax_query' => array(
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'slug',
                    'terms' => $sub_cat,
                )
            )
        )
    );

    $result=array();
    if (count($result_posts)>0)
        foreach ($result_posts as $value){
            $value=(array)$value;
            $meta=get_metadata('post',$value['ID'],'_thumbnail_id',true);
            $meta=wp_get_attachment_url($meta);
            if ($add_thumbnail)
                $result[]=array_merge($value,array('thumbnail'=>((!$meta)?'':$meta)));
            else  $result[]=$value;
        }
    return $result;
}

function GetImageByCategory($category_id=0,$key='category-image-id',$image_size='full'){
    $image_id = get_term_meta ( $category_id, $key, true );
    if ( $image_id )
        return image_downsize( $image_id ,$image_size)[0];
    else return false;
}

function do_excerpt($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if (count($words) > $word_limit)
        array_pop($words);
    echo implode(' ', $words).' ...';
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}


class description_walker extends Walker_Nav_Menu
{
    function start_el(&$output, $item, $depth, $args)
    {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $prepend = '<strong>';
        $append = '</strong>';
        $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

        if($depth != 0)
        {
            $description = $append = $prepend = "";
        }

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
        $item_output .= $description.$args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

add_theme_support( 'html5', array( 'search-form' ) );

function my_search_form( $form ) {
    $form = '
        <form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
            <button class="send" type="button">
                <img src="'. get_template_directory_uri() .'/app/img/search.png" alt="">
            </button>
            <input type="text" autocomplete="off" value="' . get_search_query() . '" name="s" id="s" placeholder="Find your question"/>
            <ul class="search_result">
   
            </ul>
        </form>'
    ;
    return $form;
}

add_filter( 'get_search_form', 'my_search_form' );

function pindify_post_types(){

    //====================Clients
    $labels = array(
        'name'                => '',
        'singular_name'       => '',
        'add_new'             => 'Додати',
        'add_new_item'        => 'Додати новий',
        'edit_item'           => 'Редагувати',
        'new_item'            => 'Додати',
        'all_items'           => 'Все',
        'view_item'           => 'Переглянути',
        'search_items'        => 'Шукаємо',
        'not_found'           => 'Не знайдено',
        'not_found_in_trash'  => 'Не знайдено у смітнику',
        'menu_name'           => 'Рубрики',
    );

    $supports = array('title', 'thumbnail','editor');

    $slug = get_theme_mod( 'rubrics_permalink' );
    $slug = ( empty( $slug ) ) ? 'rubrics' : $slug;

    $args = array(
        'labels'              => $labels,
        'public'              => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'query_var'           => true,
        'rewrite'             => array( 'slug' => $slug ),
        'capability_type'     => 'post',
        'has_archive'         => false,
        'hierarchical'        => true,
        'menu_position'       => 5,
        'taxonomies'          => array('category'),
        'supports'            => $supports,
    );

    register_post_type( 'rubrics', $args );
    register_taxonomy( 'rubrics', 'rubrics', array(
        'label'        => 'Rubrics',
        'labels'       => array(
            'menu_name' => __( 'Rubrics', 'pindify' )
        ),
        'rewrite'      => array(
            'slug' => 'rubrics',//'clients',
            'hierarchical' => false
        ),
        'hierarchical' => false,
        'show_in_menu' => false,// adding to custom menu manually
        'query_var' => true,
        'public'=>true
    ) );
    add_rewrite_tag( "%rubrics%", '([^/]+)', "post_type=rubrics&name=" );
    add_permastruct('rubrics', 'rubrics/%rubrics%', array(
        'with_front'  => true,
        'paged'       => true,
        'ep_mask'     => EP_NONE,
        'feed'        => false,
        'forcomments' => false,
        'walk_dirs'   => false,
        'endpoints'   => false,
    ));
}
add_action( 'init', 'pindify_post_types' );

/**
 * Ajax for search form
 *
 */
add_action( 'wp_ajax_search_helper', 'search_by_part' );
add_action( 'wp_ajax_nopriv_search_helper', 'search_by_part' );
function search_by_part(){
    # valiadtors
    $data=filter_input(INPUT_POST,'data',FILTER_DEFAULT ,FILTER_REQUIRE_ARRAY);
    if (empty($data)) ajax_response(false,'Bad Data');
    $temp_data = array();
    try{
        $temp_data=array(
            "search"=>$data['search'],
            );
    }
    catch (Exception $e){ ajax_response(false,'Bad Data'); }

    $filter_args=array(
        "search"=>FILTER_SANITIZE_STRING,
    );

    $data=filter_var_array(
        $temp_data,
        $filter_args,
        true);

    if (strlen($data["search"]) <1 )
        ajax_response(false,'Too short keyword');
    global $wpdb;
    $sql=$wpdb->prepare(
        "SELECT DISTINCT * FROM `wp_posts` WHERE post_type='rubrics' and post_status='publish' and post_title like %s;",
        '%'.$data["search"].'%');
//    $sql=$wpdb->prepare(
//    "SELECT * FROM `wp_posts` WHERE post_type='rubrics' and post_status='publish' and match (post_title, post_content) against(%s  IN BOOLEAN MODE);",
//    $data["search"]);
    $query_result=$wpdb->get_results($sql,ARRAY_A);
    $result = array();
    foreach ($query_result as $key=>$value){
        $result[]=array(
            'postId'=>$value["ID"],
            'url'   => get_permalink($value["ID"]),
            'post_title' =>$value["post_title"]
            );
    }
    ajax_response(true,$result);
    die();
}

/**
 * REST or REST-like response
 * @param bool $status
 * @param string $data
 */
function ajax_response($status=true,$data=''){
    echo json_encode(array("status"=>$status,"data"=>$data));
    die();
}

function my_cloud($echo = false) {
    if (function_exists('wp_tag_cloud'))
        return wp_tag_cloud();
}